package ua.khpi.ropay.final_task.database;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ua.khpi.ropay.final_task.database.dao.TestAttributeDAO;
import ua.khpi.ropay.final_task.database.dao.TestOrderDAO;
import ua.khpi.ropay.final_task.database.dao.TestServiceDAO;
import ua.khpi.ropay.final_task.database.dao.TestSubscriberDAO;
import ua.khpi.ropay.final_task.database.dao.TestTariffDAO;

@RunWith(Suite.class)
@SuiteClasses({TestAttributeDAO.class, TestOrderDAO.class, TestServiceDAO.class, TestSubscriberDAO.class, TestTariffDAO.class})
public class AllTests {

}
