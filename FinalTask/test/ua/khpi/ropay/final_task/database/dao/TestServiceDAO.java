package ua.khpi.ropay.final_task.database.dao;

import static org.junit.Assert.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ua.khpi.ropay.final_task.database.objects.Service;


public class TestServiceDAO {

    private static Logger logger = LogManager.getLogger(TestServiceDAO.class);

    private ServiceDAO serviceDAO = DAOFactory.getServiceDAO();

    @Test
    public void getInsertQueryMustReturnRightQuery(){
        String expected = "INSERT INTO providerdb.service (id_service, name) VALUES (?,?);";
        String actual = serviceDAO.getInsertQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateQueryMustReturnRightQuery(){
        String expected = "UPDATE providerdb.service SET service.name = ? WHERE service.id_service = ?";
        String actual = serviceDAO.getUpdateQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getSelectQueryMustReturnRightQuery(){
        String expected = "SELECT * FROM providerdb.service";
        String actual = serviceDAO.getSelectQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getDeleteQueryMustReturnRightQuery(){
        String expected = "DELETE FROM providerdb.service WHERE id_service = ?";
        String actual = serviceDAO.getDeleteQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllMustReturnRightNotNull(){
        List<Service> actual = serviceDAO.getAll();
        assertNotNull(actual);
    }

    @Test
    public void getByPKMustReturnRightResult(){
        Service service = new Service(1, "TV+");
        List<Service> actual = serviceDAO.getByKey("1", "WHERE service.id_service = ?;");
        assertEquals(service.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void insertMustInsertRightObject(){
        logger.info("TEST =====> insert service");
        Service expected = new Service(4, "ALL");
        serviceDAO.insert(expected);
        List<Service> actual = serviceDAO.getByKey(expected.getIdService() + "", "WHERE service.id_service = ?;");
        assertEquals(expected, actual.get(0));
    }

    @Test
    public void updateMustUpdateRightObject(){
        logger.info("TEST =====> update service");
        Service expected = new Service(2, "Phone+");
        serviceDAO.update(expected);
        List<Service> actual = serviceDAO.getByKey("2", "WHERE service.id_service = ?;");
        assertEquals(expected.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void deleteMustDeleteRightObject(){
        int expected = 0;
        logger.info("TEST =====> delete service");
        serviceDAO.delete("4");
        List<Service> arrList = serviceDAO.getByKey("4", "WHERE service.id_service = ?;");
        int actual = arrList.size();
        assertEquals(expected, actual);
    }
}
