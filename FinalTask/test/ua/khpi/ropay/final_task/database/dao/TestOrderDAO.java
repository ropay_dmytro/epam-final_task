package ua.khpi.ropay.final_task.database.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ua.khpi.ropay.final_task.database.objects.Order;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;

public class TestOrderDAO {

    private static Logger logger = LogManager.getLogger(TestOrderDAO.class);

    private OrderDAO orderDAO = DAOFactory.getOrderDAO();

    @Test
    public void getInsertQueryMustReturnRightQuery() {
        String expected = "INSERT INTO providerdb.order (id_order, login, tariff, date) " +
                "VALUES(?,?,?,?)";
        String actual = orderDAO.getInsertQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateQueryMustReturnRightQuery() {
        String expected = "UPDATE providerdb.order SET order.login = ?, order.tariff = ?, order.date = ? " +
                "WHERE order.id_order = ?";
        String actual = orderDAO.getUpdateQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getSelectQueryMustReturnRightQuery() {
        String expected = "SELECT * FROM providerdb.order";
        String actual = orderDAO.getSelectQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getDeleteQueryMustReturnRightQuery() {
        String expected = "DELETE FROM providerdb.order WHERE id_order = ?";
        String actual = orderDAO.getDeleteQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllMustReturnRightNotNull() {
        List<Order> actual = orderDAO.getAll();
        assertNotNull(actual);
    }

    @Test
    public void getByPKMustReturnRightResult() {
        Order order = new Order(6543, "Dima", 22, LocalDate.parse("2018-01-30"));

        List<Order> actual = orderDAO.getByKey("6543", "WHERE order.id_order = ?;");
        assertEquals(order.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void insertMustInsertRightObject() {
        logger.info("TEST =====> insert order");
        Order order = new Order(6533, "Pisha", 21, LocalDate.parse("2018-01-31"));

        orderDAO.insert(order);
        List<Order> actual = orderDAO.getByKey("6533", "WHERE order.id_order = ?;");
        logger.info(actual.get(0));
        assertEquals(order.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void updateMustUpdateRightObject() {
        logger.info("TEST =====> update order");
        Order order = new Order(6543, "Dima", 22, LocalDate.parse("2018-01-30"));
        orderDAO.update(order);
        List<Order> actual = orderDAO.getByKey("6543", "WHERE order.id_order = ?;");
        assertEquals(order.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void deleteMustDeleteRightObject() {
        int expected = 0;
        logger.info("TEST =====> delete order");
        orderDAO.delete("6533");
        List<Order> arrList = orderDAO.getByKey("6533", "WHERE order.id_order = ?;");
        int actual = arrList.size();
        assertEquals(expected, actual);
    }
}