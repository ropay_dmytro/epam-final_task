package ua.khpi.ropay.final_task.database.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;
import ua.khpi.ropay.final_task.database.objects.Attribute;

import java.util.List;

public class TestAttributeDAO {

    private static Logger logger = LogManager.getLogger(TestAttributeDAO.class);

    private AttributeDAO attributeDAO = DAOFactory.getAttributeDAO();

    @Test
    public void getInsertQueryMustReturnRightQuery(){
        String expected = "INSERT INTO providerdb.attribute(id_attribute, id_tariff, name, value) VALUES(?,?,?,?);";
        String actual = attributeDAO.getInsertQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateQueryMustReturnRightQuery(){
        String expected = "UPDATE providerdb.attribute SET id_tariff = ?, name = ?, value = ? WHERE id_attribute = ?";
        String actual = attributeDAO.getUpdateQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getSelectQueryMustReturnRightQuery(){
        String expected = "SELECT * FROM providerdb.attribute";
        String actual = attributeDAO.getSelectQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getDeleteQueryMustReturnRightQuery(){
        String expected = "DELETE FROM providerdb.attribute WHERE id_attribute = ?";
        String actual = attributeDAO.getDeleteQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllMustReturnRightNotNull(){
        List<Attribute> actual = attributeDAO.getAll();
        assertNotNull(actual);
    }

    @Test
    public void getByPKMustReturnRightResult(){
        Attribute attribute = new Attribute(111, 11, "Ко-во каналов", "50");
        List<Attribute> actual = attributeDAO.getByKey("111", "WHERE attribute.id_attribute = ?;");
        assertEquals(attribute, actual.get(0));
    }

    @Test
    public void insertMustInsertRightObject(){
        logger.info("TEST =====> insert attribute");
        Attribute attribute = new Attribute(131, 11, "Ко-во каналов", "HD");
        attributeDAO.insert(attribute);
        List<Attribute> actual = attributeDAO.getByKey("131", "WHERE attribute.id_attribute = ?;");
        logger.info(actual.get(0));
        assertEquals(attribute, actual.get(0));
    }

    @Test
    public void updateMustUpdateRightObject(){
        logger.info("TEST =====> update attribute");
        Attribute attribute = new Attribute(121, 11, "Ко-во каналов", "Full HD");
        attributeDAO.update(attribute);
        List<Attribute> actual = attributeDAO.getByKey("121", "WHERE attribute.id_attribute = ?;");
        assertEquals(attribute, actual.get(0));
    }

    @Test
    public void deleteMustDeleteRightObject(){
        int expected = 0;
        logger.info("TEST =====> delete attribute");
        attributeDAO.delete("131");
        List<Attribute> arrList= attributeDAO.getByKey("131", "WHERE attribute.id_attribute = ?;");
        int actual = arrList.size();
        assertEquals(expected, actual);
    }

}
