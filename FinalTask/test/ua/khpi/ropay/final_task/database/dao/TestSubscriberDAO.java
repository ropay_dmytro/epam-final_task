package ua.khpi.ropay.final_task.database.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.security.Security;

import java.util.List;

import static org.junit.Assert.*;

public class TestSubscriberDAO {

    private SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();

    private static Logger logger = LogManager.getLogger(TestSubscriberDAO.class);

    @Test
    public void getInsertQueryMustReturnRightQuery() {
        String expected = "INSERT INTO providerdb.users (login, password, name, last_name, phone_number, balance, status, locked) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        String actual = subscriberDAO.getInsertQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateQueryMustReturnRightQuery() {
        String expected = "UPDATE providerdb.users SET users.password = ?, users.name = ?, users.last_name = ?, " +
                "users.phone_number = ?, users.balance = ?, users.status = ?, users.locked = ? WHERE users.login = ?";
        String actual = subscriberDAO.getUpdateQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getSelectQueryMustReturnRightQuery() {
        String expected = "SELECT * FROM providerdb.users";
        String actual = subscriberDAO.getSelectQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getDeleteQueryMustReturnRightQuery() {
        String expected = "DELETE FROM providerdb.users WHERE login = ?";
        String actual = subscriberDAO.getDeleteQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllMustReturnRightNotNull() {
        List<Subscriber> actual = subscriberDAO.getAll();
        assertNotNull(actual);
    }

    @Test
    public void getByPKMustReturnRightResult() {
        Subscriber subscriber = new Subscriber("Slavik", Security.hash("123456"), "Славик",
                "Павлов", "0667571111", 200, "Admin", false);

        List<Subscriber> actual = subscriberDAO.getByKey("Slavik", "WHERE users.login = ?;");
        assertEquals(subscriber.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void insertMustInsertRightObject() {
        logger.info("TEST =====> insert subscriber");
        Subscriber expected = new Subscriber("Slavik", Security.hash("123456"), "Славик",
                "Павлов", "0667571111", 200, "Admin", false);
        subscriberDAO.insert(expected);
        List<Subscriber> actual = subscriberDAO.getByKey(expected.getLogin() + "", "WHERE users.login = ?;");
        assertEquals(expected.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void updateMustUpdateRightObject() {
        logger.info("TEST =====> update subscriber");
        Subscriber expected = new Subscriber("Pisha", Security.hash("123456"), "Миша",
                "Польшиков", "0667571222", 100, "User", false);
        subscriberDAO.update(expected);
        List<Subscriber> actual = subscriberDAO.getByKey("Pisha", "WHERE users.login = ?;");
        assertEquals(expected.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void deleteMustDeleteRightObject() {
        int expected = 0;
        logger.info("TEST =====> delete subscriber");
        subscriberDAO.delete("Slavik");
        List<Subscriber> arrList = subscriberDAO.getByKey("Slavik", "WHERE users.login = ?;");
        int actual = arrList.size();
        assertEquals(expected, actual);
    }

}