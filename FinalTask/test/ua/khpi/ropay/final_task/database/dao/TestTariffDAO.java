package ua.khpi.ropay.final_task.database.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestTariffDAO {

    private static Logger logger = LogManager.getLogger(TestTariffDAO.class);

    private TariffDAO tariffDAO = DAOFactory.getTariffDAO();

    @Test
    public void getInsertQueryMustReturnRightQuery(){
        String expected = "INSERT INTO providerdb.tariff(id_tariff, id_service, name, price) VALUES (?,?,?,?);";
        String actual = tariffDAO.getInsertQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateQueryMustReturnRightQuery(){
        String expected = "UPDATE providerdb.tariff SET id_service = ?, name = ?, price = ? WHERE id_tariff = ?";
        String actual = tariffDAO.getUpdateQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getSelectQueryMustReturnRightQuery(){
        String expected = "SELECT * FROM providerdb.tariff";
        String actual = tariffDAO.getSelectQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getDeleteQueryMustReturnRightQuery(){
        String expected = "DELETE FROM providerdb.tariff WHERE id_tariff = ?";
        String actual = tariffDAO.getDeleteQuery();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllMustReturnRightNotNull(){
        List<Tariff> actual = tariffDAO.getAll();
        assertNotNull(actual);
    }

    @Test
    public void getByPKMustReturnRightResult(){
        Tariff tariff = new Tariff(21, 2, "Безлимит", 50);
        List<Tariff> actual = tariffDAO.getByKey("21", "WHERE tariff.id_tariff = ?;");
        assertEquals(tariff.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void insertMustInsertRightObject(){
        logger.info("TEST =====> insert tariff");
        Tariff expected = new Tariff(13, 1, "120 каналов", 120);
        tariffDAO.insert(expected);
        List<Tariff> actual = tariffDAO.getByKey(expected.getIdTariff() + "", "WHERE tariff.id_tariff = ?;");
        assertEquals(expected, actual.get(0));
    }

    @Test
    public void updateMustUpdateRightObject(){
        logger.info("TEST =====> update tariff");
        Tariff expected = new Tariff(11, 1, "50 каналов", 60);
        tariffDAO.update(expected);
        List<Tariff> actual = tariffDAO.getByKey("11", "WHERE tariff.id_tariff = ?;");
        assertEquals(expected.toString().trim(), actual.get(0).toString().trim());
    }

    @Test
    public void deleteMustDeleteRightObject(){
        int expected = 0;
        logger.info("TEST =====> delete tariff");
        tariffDAO.delete("13");
        List<Tariff> arrList = tariffDAO.getByKey("13", "WHERE tariff.id_tariff = ?;");
        int actual = arrList.size();
        assertEquals(expected, actual);
    }
}
