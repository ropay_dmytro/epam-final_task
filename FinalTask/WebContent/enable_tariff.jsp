<%--
  Created by IntelliJ IDEA.
  User: Дима
  Date: 02.02.2018
  Time: 12:23
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="WEB-INF/jspf/Header.jspf" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Enable tariff</title>
</head>
<body>
<div id="modal">
    <div><fmt:message key="enable_tariff.text.want"/></div>
    <c:forEach var="tariff" items="${tariff_list}" varStatus="status">
        <c:if test="${tariff.idTariff eq param.tariff}">
            ${tariff.name}:
            <fmt:message key="enable_tariff.text.price"/>
            ${tariff.price}<br>
            <c:forEach var="attribute" items="${tariff.attributes}" varStatus="status">
                ${attribute.name} - ${attribute.value}<br>
            </c:forEach>
            <br>
            <form id="enable_tariff_form" action="Controller" method="get">
                <input type="hidden" name="command" value="enable_tariff"/>
                <input type="hidden" name="id_tariff" value="${tariff.idTariff}">
                <input type="submit" value="<fmt:message key="enable_tariff.button.enable"/>"/>
            </form>
        </c:if>
    </c:forEach>
</div>
</body>
</html>
