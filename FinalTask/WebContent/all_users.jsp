<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>All users</title>
</head>
<body>
<fmt:message key="user_list.button.block" var="block"/>
<fmt:message key="user_list.button.unblock" var="unblock"/>
<div class="main-table">
    <table>
        <tr>
            <th>Login</th>
            <th>Name</th>
            <th>Last name</th>
            <th>Phone number</th>
            <th>Balance</th>
            <th></th>
        </tr>
        <c:forEach var="user" items="${user_list}" varStatus="status">
            <tr>
                <td>${user.login}</td>
                <td>${user.name}</td>
                <td>${user.lastName}</td>
                <td>${user.phoneNumber}</td>
                <td>${user.balance}</td>
                <td>
                    <c:if test="${not user.getLocked()}">
                        <a href="Controller?command=block_user&user_login=${user.login}&is_block=true"
                           class="btn btn-enable">${block}</a>
                    </c:if>
                    <c:if test="${user.getLocked()}">
                        <a href="Controller?command=block_user&user_login=${user.login}&is_block=false"
                           class="btn btn-enable">${unblock}</a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>


</body>
</html>
