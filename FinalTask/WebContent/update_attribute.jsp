<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Title</title>
</head>
<body>
<div id="modal">
    <div><fmt:message key="service.href.update_attribute"/>: ${param.name}?</div>
    <form id="update_attribute_form" action="Controller" method="get" onsubmit="validate(event)">
        <fmt:message key="update_attribute.text.name"/><br>
        <input type="hidden" name="id_tariff" value="${param.tariff}"/>
        <input type="hidden" name="id_attribute" value="${param.attribute}"/>
        <div><input type="text" name="new_attribute_name" id="name"/></div>
        <fmt:message key="update_attribute.text.value"/> <br>
        <div><input type="text" name="new_attribute_value" id="value"/></div>
        <input type="hidden" name="command" value="update_attribute">
        <input type="submit" value="<fmt:message key="update_tariff.button.update"/>"/>
    </form>
</div>
<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const name = document.getElementById("name");

        const value = document.getElementById("value");

        resetError(name.parentNode);
        if (!name.value) {
            showError(name.parentNode, "Field \"name\" is empty!");
            event.preventDefault();
        }

        resetError(value.parentNode);
        if (!value.value) {
            showError(value.parentNode, "Field \"value\" is empty!");
            event.preventDefault();
        }
    }
</script>
</body>
</html>
