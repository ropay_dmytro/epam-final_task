<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jspf/Header.jspf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome</title>
</head>
<body>
<form id="language_form">
    <select id="language" name="language" onchange="submit()">
        <option value="ru" ${language == 'ru' ? 'selected' : ''}>Ru</option>
        <option value="en" ${language == 'en' ? 'selected' : ''}>En</option>
    </select>
</form>
<fmt:message key="login_jsp.button.submit" var="submit"/>
<fmt:message key="login_jsp.input.enter_login" var="logg"/>
<fmt:message key="login_jsp.input.enter_password" var="pass"/>
<fmt:message key="login_jsp.input.empty_login" var="empty_login"/>
<fmt:message key="login_jsp.input.empty_password" var="empty_pass"/>

<div id="modal">
    <form id="login_form" action="Controller" method="POST" onSubmit="validate(event)">
        <input type="hidden" name="command" value="login">
        <div>
            <label for="login_form"><fmt:message key="login_jsp.text_area.enter_login"/></label>
            <input type="text" name="user_login" id="user-login" placeholder="${logg}">
        </div>

        <div>
            <label for="login_form"><fmt:message key="login_jsp.text_area.enter_password"/></label>
            <input type="password" name="user_password" id="user-password" placeholder="${pass}">
        </div>
        <input type="submit" value="${submit}"><br>
    </form>
</div>

<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const user = document.getElementById("user-login");

        const password = document.getElementById("user-password");
        resetError(user.parentNode);

        if (!user.value) {
            showError(user.parentNode, "${empty_login}");
            event.preventDefault();
        }

        resetError(password.parentNode);
        if (!password.value) {
            showError(password.parentNode, "${empty_pass}");
            event.preventDefault();
        }
    }
</script>
</body>
</html>