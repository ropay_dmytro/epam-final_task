<%@ include file="WEB-INF/jspf/Header.jspf" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Add balance</title>
</head>
<body>
<fmt:message key="add_attribute.button.submit" var="submit"/>
<fmt:message key="add_attribute.error.name" var="empty-name"/>
<fmt:message key="add_attribute.button.submit" var="submit"/>
<div id="modal">
    <div><fmt:message key="home.button.add_balence"/>: </div>
    <form id="balance-form" action="Controller" method="post" onsubmit="validate(event)">
        <input type="hidden" name="user_login" value="${user.login}"/>
        <input type="hidden" name="command" value="add_balance">
        <div><input type="text" name="pay" id="pay"></div>
        <input type="submit" value="${submit}">
    </form>
</div>
<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const pay = document.getElementById("pay");

        resetError(pay.parentNode);

        if (!pay.value) {
            showError(pay.parentNode, "Field \"pay\" is empty!");
            event.preventDefault();
        }

        resetError(pay.parentNode);

        if (!/^[0-9]+$/.test(pay.value)) {
            showError(pay.parentNode, "Not valid number!");
            event.preventDefault();
        }
    }
</script>
</body>
</html>
