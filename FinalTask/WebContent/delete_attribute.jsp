<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Title</title>
</head>
<body>
<div id="modal">
<fmt:message key="delete_attribute.text.delete"/> ${param.name}?<br>
<form id="update_attribute_form" action="Controller" method="get">
    <input type="hidden" name="id_attribute" value="${param.attribute}"/>
    <input type="hidden" name="command" value="delete_attribute">
    <input type="submit" value="<fmt:message key="delete_tariff.button.submit"/>"/>
    <a href="tariff.jsp?tariff=${param.tariff}"><fmt:message key="delete_tariff.button.cancel"/> </a>
</form>
</div>
</body>
</html>
