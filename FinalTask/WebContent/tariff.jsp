<%--
  Created by IntelliJ IDEA.
  User: Дима
  Date: 01.02.2018
  Time: 20:24
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="WEB-INF/jspf/Header.jspf" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Title</title>
</head>
<body>
<r:role>
    <a href="add_tariff.jsp?service=${param.tariff}" class="btn btn-update" id="btn-add-service"><fmt:message key="service.href.add_tariff"/></a>
</r:role>
<c:forEach var="elem" items="${service_list}" varStatus="status">

    <c:if test="${elem.idService == param.tariff}">
        <div>
            <c:forEach var="tariff" items="${elem.tariffs}" varStatus="status">
                <div class="tariff">
                    <span>${tariff.name}:</span><br>
                    <span>Price: ${tariff.price}</span>
                    <r:role>
                    <a href="add_attribute.jsp?tariff=${tariff.idTariff}"><img src="add.png" alt=""></a><br>
                    </r:role>
                    <c:forEach var="attribute" items="${tariff.attributes}" varStatus="status">
                        <div>
                        ${attribute.name} - ${attribute.value}
                        <r:role>
                            <a href="update_attribute.jsp?name=${attribute.name}&tariff=${attribute.idTariff}&attribute=${attribute.idAttribute}"><img src="edit.png" alt=""></a>
                            <a href="delete_attribute.jsp?name=${attribute.name}&tariff=${attribute.idTariff}&attribute=${attribute.idAttribute}"><img src="delete.png" alt=""></a>
                        </r:role>
                        </div>
                    </c:forEach>
                    <div class="buttons">
                        <c:if test="${not empty user && !user.getLocked()}">
                            <a href="enable_tariff.jsp?tariff=${tariff.idTariff}" class="btn btn-enable"><fmt:message key="service.button.enable"/></a>
                        </c:if>

                        <r:role>
                            <a href="update_tariff.jsp?tariff=${tariff.idTariff}&service=${tariff.idService}&name=${tariff.name}"
                               class="btn btn-update"><fmt:message key="service.button.update"/></a>
                            <a href="delete_tariff.jsp?tariff=${tariff.idTariff}&name=${tariff.name}"
                               class="btn btn-delete"><fmt:message key="service.button.delete"/></a>
                        </r:role>
                    </div>
                </div>
            </c:forEach>
            <a href="Controller?id_service=${param.tariff}&command=get_txt_file" class="btn btn-enable" id="btn-download">Download</a>
        </div>
    </c:if>
</c:forEach>

</body>
</html>
