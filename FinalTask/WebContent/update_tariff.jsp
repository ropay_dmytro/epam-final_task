<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Title</title>
</head>
<body>
<fmt:message key="update_tariff.error.name" var="error-name"/>
<div id="modal">
        <div><fmt:message key="update_tariff.text.change"/> <b>${param.name}</b></div>
        <form id="update_tariff_form" action="Controller" method="get" onSubmit="validate(event)">
            <fmt:message key="update_tariff.text.name"/><br>
            <input type="hidden" name="id_tariff" value="${param.tariff}"/>
            <input type="hidden" name="id_service" value="${param.service}"/>
           <div><input type="text" name="new_tariff_name" id="tariff"/></div>

            <div><span><fmt:message key="update_tariff.text.price"/> </span><br>
            <input type="text" name="new_tariff_price" id="price"/></div>
            <input type="hidden" name="command" value="update_tariff">
            <input type="submit" value="<fmt:message key="update_tariff.button.update"/>"/>
        </form>
</div>
    <script>
        function showError(container, errorMessage) {
            container.className = 'login_form_error';
            const msgElem = document.createElement('span');
            msgElem.className = "error-message";
            msgElem.innerHTML = errorMessage;
            container.appendChild(msgElem);
        }

        function resetError(container) {
            container.className = '';
            if (container.lastChild.className == "error-message") {
                container.removeChild(container.lastChild);
            }
        }

        function validate(event) {
            const tariff = document.getElementById("tariff");

            const price = document.getElementById("price");
            resetError(tariff.parentNode);

            if (!tariff.value) {
                showError(tariff.parentNode, "Field \"name\" is empty!");
                event.preventDefault();
            }

            resetError(price.parentNode);
            if (!price.value) {
                showError(price.parentNode, "Field \"price\" is empty!");
                event.preventDefault();
            }

            resetError(price.parentNode);
            if (price.value < 0 || price.value == 0) {
                showError(price.parentNode, " < 0");
                event.preventDefault();
            }
        }
    </script>
</body>
</html>
