<%@ include file="WEB-INF/jspf/Header.jspf" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Proffile</title>
</head>
<body>

<div id="modal">
    <div><fmt:message key="proffile.text.login"/> ${user.login}</div>
    <div><fmt:message key="proffile.text.name"/> ${user.name}</div>
    <div><fmt:message key="proffile.text.balance"/> ${user.balance} <a href="add_balance.jsp"><fmt:message key="home.button.add_balence"/></a></div>
    <div><fmt:message key="proffile.text.status"/> ${user.status}</div>
    <div><fmt:message key="proffile.text.number"/> <a href="tel: ${user.phoneNumber}">${user.phoneNumber}</a></div>
    <div><fmt:message key="proffile.text.blocked"/> ${user.getLocked()}</div>
    <br>
    <div><fmt:message key="proffile.text.tariffs"/></div>
    <c:forEach var="tariff" items="${user_tariffs}" varStatus="status">
        <div>
            <fmt:message key="add_tariff.text.name"/>
            <b>${tariff.name}</b>
            <fmt:message key="add_tariff.text.price"/>
                ${tariff.price}
        </div>

    </c:forEach>
</div>
</body>
</html>
