<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Add tariff</title>
</head>
<body>
<fmt:message key="add_tariff.button.submit" var="submit"/>
<fmt:message key="add_tariff.error.name" var="name"/>
<fmt:message key="add_tariff.error.price" var="price"/>
<div id="modal">
    <form id="add_tariff_form" action="Controller" method="get" onSubmit="validate(event)">
        <input type="hidden" name="command" value="add_tariff">
        <input type="hidden" name="service" value="${param.service}">
        <div><span><fmt:message key="add_tariff.text.name"/><br></span>
            <input type="text" name="new_tariff_name" id="name"/></div>
        <div><fmt:message key="add_tariff.text.price"/><br>
            <input type="text" name="new_tariff_price" id="price"/></div>
        <input type="submit" name="${submit}"/>
    </form>
</div>
<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const name = document.getElementById("name");

        const price = document.getElementById("price");
        resetError(name.parentNode);

        if (!name.value) {
            showError(name.parentNode, "Field \"name\" is empty!"
            event.preventDefault();
        }

        resetError(price.parentNode);
        if (!price.value) {
            showError(price.parentNode, "Field \"price\" is empty!");
            event.preventDefault();
        }

        resetError(price.parentNode);
        if (price.value == 0 || price.value < 0) {
            showError(price.parentNode, "Not valid number!");
            event.preventDefault();
        }
    }
</script>

</body>
</html>
