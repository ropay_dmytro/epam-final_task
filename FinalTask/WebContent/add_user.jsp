<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Add user</title>
</head>
<body>

<fmt:message key="add_user.button.add_user" var="add_user"/>
<div class="tariff">
    <form id="add_user_form" action="Controller" method="POST" onSubmit="validate(event)">
        <input type="hidden" name="command" value="add_user">
        <div><fmt:message key="add_user.text_area.enter_login"/> <br>
            <input type="text" name="user_login" id="login"></div>
        <div><fmt:message key="add_user.text_area.enter_password"/> <br>
            <input type="password" name="user_password_first" id="password"></div>
        <div><fmt:message key="add_user.text_area.repeat_password"/> <br>
            <input type="password" name="user_password" id="repeat_password"></div>
        <div><fmt:message key="add_user.text_area.enter_user_name"/> <br>
            <input type="text" name="user_name" id="name"></div>
        <div><fmt:message key="add_user.text_area.enter_last_name"/> <br>
            <input type="text" name="user_last_name" id="last-name"></div>
        <div><fmt:message key="add_user.text_area.enter_phone_number"/> <br>
            <input type="text" name="user_phone_number" id="phone"></div>
        <div><fmt:message key="add_user.text_area.enter_user_status"/> <br>
            <input type="radio" name="user_status" id="user-status-user" value="User" id="user">
            <label for="user-status-user"><fmt:message key="add_user.text_area.user"/></label>
        </div>
        <div><input type="radio" name="user_status" id="user-status-admin" value="Admin" id="admin">
            <label for="user-status-admin"><fmt:message
                    key="add_user.text_area.admin"/></label></div>

        <input type="submit" value="${add_user}"><br>
    </form>
</div>
</div>
<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const login = document.getElementById("login");
        const password = document.getElementById("password");
        const passwordRepeat = document.getElementById("repeat_password");
        const name = document.getElementById("name");
        const lastName = document.getElementById("last-name");
        const phone = document.getElementById("phone");
        const user = document.getElementById("user");
        const admin = document.getElementById("admin");

        resetError(login.parentNode);

        if (!login.value) {
            showError(login.parentNode, "Field \"login\" is empty valid!");
            event.preventDefault();
        }

        resetError(password.parentNode);
        resetError(passwordRepeat.parentNode);
        if (!password.value) {
            showError(password.parentNode, "Field \"password\" is empty!");
            event.preventDefault();
        } else if (password.value != passwordRepeat.value) {
            showError(password.parentNode, "Not valid");
            showError(passwordRepeat.parentNode, "Not valid");
            event.preventDefault();
        }

        resetError(name.parentNode);
        if (!name.value) {
            showError(name.parentNode, "Field \"name\" is empty!");
            event.preventDefault();
        }

        resetError(lastName.parentNode);
        if (!lastName.value) {
            showError(lastName.parentNode, "Field \"last name\" is empty!");
            event.preventDefault();
        }

        resetError(phone.parentNode);
        if (!phone.value) {
            showError(phone.parentNode, "Field \"phone\" is empty!");
            event.preventDefault();
        }

        resetError(admin.parentNode);
        if (!(admin.value && user.value)) {
            showError(lastName.parentNode, "Field \"status\" is empty!");
            event.preventDefault();
        }
    }
</script>
</body>
</html>
