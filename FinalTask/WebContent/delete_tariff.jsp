<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Delete tariff</title>
</head>
<body>
<div id="modal">
    <div><fmt:message key="delete_tariff.text.delete"/> ${param.name}?</div>
    <form id="update_tariff_form" action="Controller" method="get">
        <input type="hidden" name="id_tariff" value="${param.tariff}"/>
        <input type="hidden" name="command" value="delete_tariff">
        <input type="submit" value="<fmt:message key="delete_tariff.button.submit"/>"/>
        <a href="tariff.jsp?tariff=${param.tariff}"><fmt:message key="delete_tariff.button.cancel"/></a>
    </form>
</div>

</body>
</html>
