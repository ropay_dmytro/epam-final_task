<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Welcome</title>
</head>
<body>


<%@ include file="/WEB-INF/jspf/Header.jspf" %>
<form id="language_form">
    <select id="language" name="language" onchange="submit()">
        <option value="ru" ${language == 'ru' ? 'selected' : ''}>Ru</option>
        <option value="en" ${language == 'en' ? 'selected' : ''}>En</option>
    </select>
</form>

<div id="main-heading">
    <h1><fmt:message key="home.text_area.welcome"/> ${user.name}!</h1>
    <h3 style="text-align: center"><a href="Controller?command=task_command" style="align-content: center">Go to task!</a></h3>
    <c:if test="${empty user}">
        <p><fmt:message key="home.text.welcome"/></p>
    </c:if>
</div>

</body>
</html>
