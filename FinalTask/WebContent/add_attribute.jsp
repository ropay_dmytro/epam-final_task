<%@ include file="WEB-INF/jspf/Header.jspf" %>
<r:permit/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <title>Title</title>
</head>
<body>
<fmt:message key="add_attribute.error.name" var="empty_name"/>
<fmt:message key="add_attribute.error.value" var="empty_value"/>
<fmt:message key="add_attribute.button.submit" var="submit"/>
<div id="modal">
<form id="add_attribute_form" action="Controller" method="get" onsubmit="validate(event)">
    <input type="hidden" name="command" value="add_attribute">
    <input type="hidden" name="id_tariff" value="${param.tariff}">
    <fmt:message key="add_attribute.text.name"/><br>
    <div><input type="text" name="attribute_name" id="name"/></div>
    <fmt:message key="add_attribute.text.value"/><br>
    <div><input type="text" name="attribute_value" id="value"/></div>
    <input type="submit" name="${submit}"/>
</form>
</div>
<script>
    function showError(container, errorMessage) {
        container.className = 'login_form_error';
        const msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    function resetError(container) {
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
    }

    function validate(event) {
        const name = document.getElementById("name");

        const value = document.getElementById("value");

        resetError(name.parentNode);
        if (!name.value) {
            showError(name.parentNode, "Field \"name\" is empty!");
            event.preventDefault();
        }

        resetError(value.parentNode);
        if (!value.value) {
            showError(value.parentNode, "Field \"value\" is empty!");
            event.preventDefault();
        }
    }
</script>
</body>
</html>
