package ua.khpi.ropay.final_task.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.command.Receiver;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static Logger logger = LogManager.getLogger(Controller.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        logger.info("Servlet constructor");
    }

    @Override
    public void init() throws ServletException {
        super.init();
        logger.info("Servlet init()");

    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Command command = Receiver.get(request.getParameter("command"));

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if(command!=null) {
            logger.info("Execute command");
            command.execute(request, response);
        } else {
            logger.info("Command = null!");
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }



}
