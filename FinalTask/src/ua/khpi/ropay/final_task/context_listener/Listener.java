package ua.khpi.ropay.final_task.context_listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.ServiceDAO;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Service;
import ua.khpi.ropay.final_task.database.objects.Tariff;
import ua.khpi.ropay.final_task.timer_task.MyTimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.*;

@WebListener()
public class Listener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    private static Logger logger = LogManager.getLogger(Listener.class);

    // Public constructor is required by servlet spec
    public Listener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {
      /* This method is called when the servlet context is
         initialized(when the Web application is deployed). 
         You can initialize servlet context related data here.
      */
        logger.info("contextInitialized");
        startTimerTask();
        loadServiceList(sce.getServletContext());

    }

    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         Application Server shuts down.
      */
        logger.info("contextDestroyed");
        //ServletContext servletContext = sce.getServletContext();
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
      /* Session is created. */
        logger.info("sessionCreated");
    }

    public void sessionDestroyed(HttpSessionEvent se) {
      /* Session is destroyed. */
        logger.info("sessionDestroyed");
    }

    // -------------------------------------------------------
    // HttpSessionAttributeListener implementation
    // -------------------------------------------------------

    public void attributeAdded(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute 
         is added to a session.
      */
    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute
         is removed from a session.
      */
    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {
      /* This method is invoked when an attibute
         is replaced in a session.
      */
    }

    public void loadServiceList(ServletContext servletContext) {
        logger.info("Write data to context");
        ServiceDAO serviceDAO = DAOFactory.getServiceDAO();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        List<Service> serviceList = serviceDAO.getAll();
        List<Tariff> tariffList = tariffDAO.getAll();
        servletContext.setAttribute("service_list", serviceList);
        servletContext.setAttribute("tariff_list", tariffList);
    }

    private void startTimerTask(){
        logger.info("Start timer task");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 02);
        cal.set(Calendar.MINUTE, 00);
        Date date = cal.getTime();
        Timer timer = new Timer();
        MyTimerTask timerTask = new MyTimerTask();

        timer.schedule(timerTask, date);
    }
}
