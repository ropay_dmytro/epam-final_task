package ua.khpi.ropay.final_task.my_tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.database.objects.Subscriber;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class RoleTag extends TagSupport {
    private static Logger logger = LogManager.getLogger(RoleTag.class);


    @Override
    public int doStartTag() throws JspException{
        HttpSession session = pageContext.getSession();
        Subscriber subscriber = (Subscriber) session.getAttribute("user");
        if(subscriber != null && subscriber.getStatus().equals("Admin")){
            logger.info("true");
            return EVAL_BODY_INCLUDE;
        } else {
            logger.info("false");
            return SKIP_BODY;
        }
    }


}
