package ua.khpi.ropay.final_task.my_tag;

import ua.khpi.ropay.final_task.database.objects.Subscriber;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class PermissionTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        Subscriber subscriber = (Subscriber) session.getAttribute("user");

        if(subscriber == null || !subscriber.getStatus().equals("Admin")){
          
            HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
            try {
                response.sendRedirect("permission_error.jsp");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return super.doStartTag();
    }

}
