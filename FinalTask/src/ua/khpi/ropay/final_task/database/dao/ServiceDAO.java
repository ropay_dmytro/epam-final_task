package ua.khpi.ropay.final_task.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.khpi.ropay.final_task.database.objects.Service;
import ua.khpi.ropay.final_task.database.objects.Tariff;

public class ServiceDAO extends AbstractController<Service> {

	private Connection connection;

	public ServiceDAO(Connection connection) {
		super(connection);
		this.connection = connection;
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO providerdb.service (id_service, name) VALUES (?,?);";
	}

	@Override
	protected String getSelectQuery() {
		return "SELECT * FROM providerdb.service";
	}

	@Override
	protected String getDeleteQuery() {
		return "DELETE FROM providerdb.service WHERE id_service = ?";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE providerdb.service SET service.name = ? WHERE service.id_service = ?";
	}

	public List<Service> parseResultSet(ResultSet resultSet) {
		List<Service> result = new ArrayList<>();
		TariffDAO tariffDAO = new TariffDAO(connection);
		try {
			while (resultSet.next()) {
				Service service = new Service();
				service.setIdService(resultSet.getInt("service.id_service"));
				service.setName(resultSet.getString("service.name"));
				service.setTariffs(
						(ArrayList<Tariff>) tariffDAO.getByKey(service.getIdService()+"", "WHERE tariff.id_service = ?"));
				result.add(service);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Service object) {
		try {
			statement.setInt(1, object.getIdService());
			statement.setString(2, object.getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Service object) {
		try {
			statement.setString(1, object.getName());
			statement.setInt(2, object.getIdService());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
