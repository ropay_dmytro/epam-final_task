package ua.khpi.ropay.final_task.database.dao;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class DAOFactory {

   private final static Logger logger = LogManager.getLogger(DAOFactory.class);
    private static Connection connection = null;
    private static MysqlDataSource mysqlDataSource = null;

    static {
        try {
            Class.forName(Constant.DRIVER);
            logger.info("Load driver");
        } catch (ClassNotFoundException e) {
            logger.error("Driver not found");
            e.printStackTrace();
        }
    }

    public static SubscriberDAO getSubscriberDAO(){
        SubscriberDAO subscriberDAO = new SubscriberDAO(getConnectionPool());
        return subscriberDAO;
    }

    public static ServiceDAO getServiceDAO(){
        ServiceDAO serviceDAO = new ServiceDAO(getConnectionPool());
        return serviceDAO;
    }

    public static TariffDAO getTariffDAO(){
        TariffDAO tariffDAO = new TariffDAO(getConnectionPool());
        return tariffDAO;
    }

    public static AttributeDAO getAttributeDAO(){
        AttributeDAO attributeDAO = new AttributeDAO(getConnectionPool());
        return attributeDAO;
    }

    public static OrderDAO getOrderDAO(){
        OrderDAO orderDAO = new OrderDAO(getConnectionPool());
        return orderDAO;
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(Constant.DB_URL, Constant.LOGIN, Constant.PASSWORD);
                logger.info("Created new connection");
            } catch (SQLException e) {
                logger.error("Error connection from BD");
                e.printStackTrace();
            }
            return connection;
        } else {
            logger.info("Return old connection");
            return connection;
        }
    }

    public static Connection getConnectionPool(){
        mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setURL(Constant.DB_URL);
        mysqlDataSource.setUser(Constant.LOGIN);
        mysqlDataSource.setPassword(Constant.PASSWORD);
        mysqlDataSource.setPort(3306);
        try {
            connection = mysqlDataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
