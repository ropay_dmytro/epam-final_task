package ua.khpi.ropay.final_task.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.khpi.ropay.final_task.database.objects.Attribute;
import ua.khpi.ropay.final_task.database.objects.Tariff;

public class TariffDAO extends AbstractController<Tariff> {

	private Connection connection;

	public TariffDAO(Connection connection) {
		super(connection);
		this.connection = connection;
	}

	@Override
	protected String getInsertQuery() {

		return "INSERT INTO providerdb.tariff(id_service, name, price) VALUES (?,?,?);";
	}

	@Override
	protected String getSelectQuery() {

		return "SELECT * FROM providerdb.tariff";
	}

	@Override
	protected String getDeleteQuery() {

		return "DELETE FROM providerdb.tariff WHERE id_tariff = ?";
	}

	@Override
	protected String getUpdateQuery() {

		return "UPDATE providerdb.tariff SET id_service = ?, name = ?, price = ? WHERE id_tariff = ?";
	}

	@Override
	protected List<Tariff> parseResultSet(ResultSet resultSet) {
		ArrayList<Tariff> result = new ArrayList<>();
		AttributeDAO attrDAO = new AttributeDAO(connection);

		try {
			while (resultSet.next()) {
				Tariff tariff = new Tariff();
				tariff.setIdTariff(resultSet.getInt("tariff.id_tariff"));
				tariff.setIdService(resultSet.getInt("tariff.id_service"));
				tariff.setName(resultSet.getString("tariff.name"));
				tariff.setPrice(resultSet.getInt("tariff.price"));
				tariff.setAttributes((ArrayList<Attribute>)attrDAO.getByKey(tariff.getIdTariff() + "", "WHERE attribute.id_tariff = ?"));
				result.add(tariff);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Tariff object) {
		try {
			statement.setInt(1, object.getIdService());
			statement.setString(2, object.getName());
			statement.setInt(3, object.getPrice());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Tariff object) {
		try {
			statement.setInt(1, object.getIdService());
			statement.setString(2, object.getName());
			statement.setInt(3, object.getPrice());
			statement.setInt(4, object.getIdTariff());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
