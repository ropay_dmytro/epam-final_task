package ua.khpi.ropay.final_task.database.dao;

public final class Constant {
	
	public static final String DB_URL = "jdbc:mysql://localhost:3306/providerdb?useSSL=false";
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String LOGIN = "root";
	public static final String PASSWORD = "31415";
	
	private Constant() {
		throw new RuntimeException();
	}

}
