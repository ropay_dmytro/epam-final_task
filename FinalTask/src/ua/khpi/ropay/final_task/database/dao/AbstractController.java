package ua.khpi.ropay.final_task.database.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public abstract class AbstractController<T> {

    private static Logger logger = LogManager.getLogger(AbstractController.class);

    private Connection connection;

    public AbstractController(Connection connection) {
        this.connection = connection;
    }
    /**
     * Returns a sql query to insert a new record into the database.
     *
     * @return string representation of query INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     */
    protected abstract String getInsertQuery();
    /**
     *  Returns a sql query to retrieve all records.
     *
     * @return string representation of query SELECT * FROM [Table]      
     */
    protected abstract String getSelectQuery();
    /**
     * Returns a sql query to delete record from database
     *
     * @return string representation of query DELETE FROM [Table] WHERE id= ?;
     */
    protected abstract String getDeleteQuery();
    /**
     * Returns a sql query to update a record in the database.
     *
     * @return string representation of query UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     */
    protected abstract String getUpdateQuery();
    /**
    * Parses the ResultSet and returns a list of objects corresponding to the contents of the ResultSet.
    *
    * @param rs result set which we parse
    * @return list of objects of type T
    */
    protected abstract List<T> parseResultSet(ResultSet resultSet);
    /**
     * Sets the insert arguments of the query according to the value of the fields of the object.
     *
     * @param statement statement to which we apply the parameters
     * @param object    the object from which we take the parameters
     */
    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object);
    /**
     * Sets the update arguments of the query according to the value of the fields of the object.
     *
     * @param statement statement to which we apply the parameters
     * @param object    the object from which we take the parameters
     */
    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object);

    public void insert(T object) {
        String query = getInsertQuery();
        logger.info("Insert query");
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            prepareStatementForInsert(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new SQLException("On persist modify more then 1 record: " + count);
            }

        } catch (SQLException e) {
            logger.error("Insert error");
            e.printStackTrace();
        }
    }

    public List<T> getAll() {
        List<T> result = null;
        String query = getSelectQuery();
        logger.info("Get all query");
        try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            ResultSet rs = prepareStatement.executeQuery();
            result = parseResultSet(rs);
        } catch (SQLException e) {
            logger.error("Get all error");
            e.printStackTrace();
        }
        return result;
    }

    public List<T> getByKey(String key, String where) {
        List<T> result = null;
        String query = getSelectQuery();
        query += " " + where;
        logger.info("Getting object by key: " + query);
        try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            prepareStatement.setString(1, key);
            ResultSet rs = prepareStatement.executeQuery();
            result = parseResultSet(rs);
        } catch (SQLException e) {
            logger.error("SQLException get by PK: ", e);
            e.printStackTrace();
        }
        return result;
    }

    public void delete(String id) {
        String query = getDeleteQuery();
        logger.info("Delete object");
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setObject(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Delete error");
            e.printStackTrace();
        }
    }

    public void update(T object) {
        String query = getUpdateQuery();
        logger.info("Update object");
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            prepareStatementForUpdate(statement, object);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
