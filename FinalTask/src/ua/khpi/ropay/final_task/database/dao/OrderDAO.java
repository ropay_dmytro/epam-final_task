package ua.khpi.ropay.final_task.database.dao;

import ua.khpi.ropay.final_task.database.objects.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Дима on 24.01.2018.
 */
public class OrderDAO extends AbstractController<Order>{

    Logger logger = LogManager.getLogger(OrderDAO.class);

    public OrderDAO(Connection connection) {
        super(connection);
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO providerdb.order (login, tariff, date) " +
                "VALUES(?,?,?)";
    }

    @Override
    protected String getSelectQuery() {
        return "SELECT * FROM providerdb.order";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM providerdb.order WHERE id_order = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE providerdb.order SET order.login = ?, order.tariff = ?, order.date = ? " +
                "WHERE order.id_order = ?";
    }

    @Override
    protected List<Order> parseResultSet(ResultSet resultSet) {
        List<Order> result = new ArrayList<>();
        logger.info("Парсинг резалт сета заказа");
        try {
            while (resultSet.next()){
                Order order = new Order();
                order.setIdOrder(resultSet.getInt("order.id_order"));
                order.setLogin(resultSet.getString("order.login"));
                order.setTariff(resultSet.getInt("order.tariff"));
                order.setOrderDate(resultSet.getDate("order.date").toLocalDate());
                result.add(order);
            }
        } catch (SQLException e) {
            logger.error("Ошибка парса rezult set заказа");
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Order object) {
        logger.info("prepareStatementForInsert");
        try {
            statement.setString(1, object.getLogin());
            statement.setInt(2, object.getTariff());
            statement.setDate(3, Date.valueOf(object.getOrderDate()));
        } catch (SQLException e) {
            logger.error("prepareStatementForInsert");
            e.printStackTrace();
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Order object) {
        try {
            logger.info("prepareStatementForUpdate");
            statement.setString(1, object.getLogin());
            statement.setInt(2, object.getTariff());
            statement.setDate(3,Date.valueOf(object.getOrderDate()));
            statement.setInt(4, object.getIdOrder());
        } catch (SQLException e) {
            logger.info("prepareStatementForUpdate");
            e.printStackTrace();
        }

    }
}
