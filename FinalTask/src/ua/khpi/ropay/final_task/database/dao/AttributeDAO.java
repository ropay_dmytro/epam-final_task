package ua.khpi.ropay.final_task.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.database.objects.Attribute;

public class AttributeDAO extends AbstractController<Attribute> {

	private static Logger logger = LogManager.getLogger(AttributeDAO.class);

	public AttributeDAO(Connection connection) {
		super(connection);
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO providerdb.attribute(id_tariff, name, value) VALUES(?,?,?);";
	}

	@Override
	protected String getSelectQuery() {
		return "SELECT * FROM providerdb.attribute";
	}

	@Override
	protected String getDeleteQuery() {
		return "DELETE FROM providerdb.attribute WHERE id_attribute = ?";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE providerdb.attribute SET id_tariff = ?, name = ?, value = ? WHERE id_attribute = ?";
	}

	@Override
	protected List<Attribute> parseResultSet(ResultSet resultSet) {
		ArrayList<Attribute> result = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Attribute attr = new Attribute();
				attr.setIdAttribute(resultSet.getInt("attribute.id_attribute"));
				attr.setIdTariff(resultSet.getInt("attribute.id_tariff"));
				attr.setName(resultSet.getString("attribute.name"));
				attr.setValue(resultSet.getString("attribute.value"));
				result.add(attr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Attribute object) {
		try {
			logger.info("Prepare statement for insert");
			statement.setInt(1, object.getIdTariff());
			statement.setString(2, object.getName());
			statement.setString(3, object.getValue());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Attribute object) {
		try {
			logger.info("Prepare statement for attribute");
			statement.setInt(1, object.getIdTariff());
			statement.setString(2, object.getName());
			statement.setString(3, object.getValue());
			statement.setInt(4, object.getIdAttribute());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
