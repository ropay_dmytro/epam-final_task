package ua.khpi.ropay.final_task.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ua.khpi.ropay.final_task.database.objects.Subscriber;

public class SubscriberDAO extends AbstractController<Subscriber> {

    static Logger logger = LogManager.getLogger(SubscriberDAO.class);

    public SubscriberDAO(Connection connection) {
        super(connection);
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO providerdb.users (login, password, name, last_name, phone_number, balance, status, locked) " +
                "VALUES (?,?,?,?,?,?,?,?)";
    }

    @Override
    protected String getSelectQuery() {
        return "SELECT * FROM providerdb.users";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM providerdb.users WHERE login = ?";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE providerdb.users SET users.password = ?, users.name = ?, users.last_name = ?, " +
                "users.phone_number = ?, users.balance = ?, users.status = ?, users.locked = ? WHERE users.login = ?";
    }

    @Override
    protected List<Subscriber> parseResultSet(ResultSet resultSet) {
        ArrayList<Subscriber> result = new ArrayList<>();
        logger.info("Parsing subscribers");
        try {
            while (resultSet.next()) {
                Subscriber subscriber = new Subscriber();
                subscriber.setLogin(resultSet.getString("users.login"));
                subscriber.setPassword(resultSet.getString("users.password"));
                subscriber.setName(resultSet.getString("users.name"));
                subscriber.setLastName(resultSet.getString("users.last_name"));
                subscriber.setPhoneNumber(resultSet.getString("users.phone_number"));
                subscriber.setBalance(resultSet.getInt("users.balance"));
                subscriber.setStatus(resultSet.getString("users.status"));
                subscriber.setLocked(resultSet.getBoolean("users.locked"));
                result.add(subscriber);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("End parsing");
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Subscriber object) {

        try {
            statement.setString(1, object.getLogin());
            statement.setString(2, object.getPassword());
            statement.setString(3, object.getName());
            statement.setString(4, object.getLastName());
            statement.setString(5, object.getPhoneNumber());
            statement.setInt(6, object.getBalance());
            statement.setString(7, object.getStatus());
            statement.setBoolean(8, object.getLocked());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Subscriber object) {

        try {
            statement.setString(1, object.getPassword());
            statement.setString(2, object.getName());
            statement.setString(3, object.getLastName());
            statement.setString(4, object.getPhoneNumber());
            statement.setInt(5, object.getBalance());
            statement.setString(6, object.getStatus());
            statement.setBoolean(7, object.getLocked());
            statement.setString(8, object.getLogin());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
