package ua.khpi.ropay.final_task.database.objects;

import java.util.ArrayList;

public class Service {

	private int idService;
	private String name;
	private ArrayList<Tariff> tariffs = new ArrayList<>();

	public Service() {
	}

	public Service(int idService, String name) {
		this.idService = idService;
		this.name = name;
	}

	public Service(int idService, String name, ArrayList<Tariff> tariffs) {
		this.idService = idService;
		this.name = name;
		this.tariffs = tariffs;
	}

	public int getIdService() {
		return idService;
	}

	public void setIdService(int idService) {
		this.idService = idService;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Tariff> getTariffs() {
		return tariffs;
	}

	public void setTariffs(ArrayList<Tariff> tariffs) {
		this.tariffs = tariffs;
	}

	public void addTariff(Tariff tariff) {
		tariffs.add(tariff);
	}

	@Override
	public String toString() {
		return "Service{" +
				"idService=" + idService +
				", name='" + name + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Service service = (Service) o;

		if (idService != service.idService) return false;
		if (name != null ? !name.equals(service.name) : service.name != null) return false;
		return tariffs != null ? tariffs.equals(service.tariffs) : service.tariffs == null;
	}

	@Override
	public int hashCode() {
		int result = idService;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (tariffs != null ? tariffs.hashCode() : 0);
		return result;
	}
}
