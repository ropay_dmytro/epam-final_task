package ua.khpi.ropay.final_task.database.objects;

import java.util.ArrayList;

public class Tariff {

    private int idTariff;
    private int idService;
    private String name;
    private ArrayList<Attribute> attributes = new ArrayList<>();
    private int price;

    public Tariff() {

    }

    public Tariff(int idService, String name, int price) {
        this.idService = idService;
        this.name = name;
        this.price = price;
    }

    public Tariff(int idTariff, int idService, String name, int price) {
        this.idTariff = idTariff;
        this.idService = idService;
        this.name = name;
        this.price = price;
    }

    public Tariff(int idTariff, int idService, String name, ArrayList<Attribute> attributes, int price) {
        this.idTariff = idTariff;
        this.idService = idService;
        this.name = name;
        this.attributes = attributes;
        this.price = price;
    }

    public int getIdTariff() {
        return idTariff;
    }

    public void setIdTariff(int idTariff) {
        this.idTariff = idTariff;
    }

    public int getIdService() {
        return idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void addAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "idTariff=" + idTariff +
                ", idService=" + idService +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tariff tariff = (Tariff) o;

        if (idTariff != tariff.idTariff) return false;
        if (idService != tariff.idService) return false;
        if (price != tariff.price) return false;
        if (name != null ? !name.equals(tariff.name) : tariff.name != null) return false;
        return attributes != null ? attributes.equals(tariff.attributes) : tariff.attributes == null;
    }

    @Override
    public int hashCode() {
        int result = idTariff;
        result = 31 * result + idService;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
        result = 31 * result + price;
        return result;
    }
}
