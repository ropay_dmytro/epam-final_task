package ua.khpi.ropay.final_task.database.objects;


import java.time.LocalDate;

/**
 * Created by Дима on 24.01.2018.
 */
public class Order {

    private int idOrder;
    private String login;
    private int tariff;
    private LocalDate orderDate;

    public Order(){

    }

    public Order(String login, int tariff, LocalDate  orderDate){
        this.login = login;
        this.tariff = tariff;
        this.orderDate = orderDate;
    }

    public Order(int idOrder, String login, int tariff, LocalDate  orderDate){
        this.idOrder = idOrder;
        this.login = login;
        this.tariff = tariff;
        this.orderDate = orderDate;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getTariff() {
        return tariff;
    }

    public void setTariff(int tariff) {
        this.tariff = tariff;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", login='" + login + '\'' +
                ", tariff='" + tariff + '\'' +
                ", orderDate=" + orderDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (idOrder != order.idOrder) return false;
        if (tariff != order.tariff) return false;
        if (login != null ? !login.equals(order.login) : order.login != null) return false;
        return orderDate != null ? orderDate.equals(order.orderDate) : order.orderDate == null;
    }

    @Override
    public int hashCode() {
        int result = idOrder;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + tariff;
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        return result;
    }
}
