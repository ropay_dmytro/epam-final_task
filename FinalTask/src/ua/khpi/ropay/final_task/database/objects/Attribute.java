package ua.khpi.ropay.final_task.database.objects;

public class Attribute {

	private int idAttribute;
	private int idTariff;
	private String name;
	private String value;

	public Attribute() {

	}

	public Attribute(int idTariff, String name, String value) {
		this.idTariff = idTariff;
		this.name = name;
		this.value = value;
	}

	public Attribute(int idAttribute, int idTariff, String name, String value) {
		this.idAttribute = idAttribute;
		this.idTariff = idTariff;
		this.name = name;
		this.value = value;
	}

	public int getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(int idAttribute) {
		this.idAttribute = idAttribute;
	}

	public int getIdTariff() {
		return idTariff;
	}

	public void setIdTariff(int idTariff) {
		this.idTariff = idTariff;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Attribute [idAttribute=" + idAttribute + ", idTariff=" + idTariff + ", name=" + name + ", value="
				+ value + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Attribute attribute = (Attribute) o;

		if (idAttribute != attribute.idAttribute) return false;
		if (idTariff != attribute.idTariff) return false;
		if (name != null ? !name.equals(attribute.name) : attribute.name != null) return false;
		return value != null ? value.equals(attribute.value) : attribute.value == null;
	}

	@Override
	public int hashCode() {
		int result = idAttribute;
		result = 31 * result + idTariff;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (value != null ? value.hashCode() : 0);
		return result;
	}
}
