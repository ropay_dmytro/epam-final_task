package ua.khpi.ropay.final_task.timer_task;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.OrderDAO;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Order;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class MyTimerTask extends TimerTask {

    private static Logger logger = LogManager.getLogger(MyTimerTask.class);

    @Override
    public void run() {
        checkingOrders();
    }

    public void checkingOrders(){
        OrderDAO orderDAO = DAOFactory.getOrderDAO();
        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        List<Order> orderList = orderDAO.getAll();
        logger.info("======> Timer task work!!!!!");
        for (Order i : orderList) {

            if (dayDifference(asDate(i.getOrderDate())) > 30) {
                Subscriber subscriber = subscriberDAO.getByKey(i.getLogin(), "WHERE users.login = ?").get(0);
                Tariff tariff = tariffDAO.getByKey(i.getTariff() + "", "WHERE tariff.id_tariff = ?").get(0);
                int balance = subscriber.getBalance() - tariff.getPrice();
                if (balance > 0) {
                    logger.info("Indebtedness from: " + subscriber.getLogin() + " from tariff: " + tariff.getName()
                            + "balance: " + balance);
                    subscriber.setBalance(balance);
                    i.setOrderDate(asLocalData(getTodayDate()));
                    orderDAO.update(i);
                    subscriber.setLocked(false);
                } else {
                    subscriber.setLocked(true);
                }
                subscriberDAO.update(subscriber);
            }
        }
    }

    private static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    private static LocalDate asLocalData(Date input) {
        LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return date;
    }

    private int dayDifference(Date orderDate) {
        long difference = getTodayDate().getTime() - orderDate.getTime();
        int days = (int) (difference / (24 * 60 * 60 * 1000));
        logger.info("Difference is: " + days);
        return days;
    }

    private Date getTodayDate() {
        String stringDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        logger.info("Today is: " + stringDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = null;
        try {
            todayDate = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return todayDate;

    }
}
