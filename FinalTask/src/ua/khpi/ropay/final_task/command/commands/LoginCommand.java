package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.database.objects.Tariff;
import ua.khpi.ropay.final_task.security.Security;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class LoginCommand implements Command {

    private static Logger logger = LogManager.getLogger(LoginCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        final String where = "WHERE users.login = ?;";

        Subscriber subscriber = new Subscriber();

        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();

        HttpSession session = request.getSession();

        logger.info("Session ID: " + session.getId());

        String login = request.getParameter("user_login");
        String password = request.getParameter("user_password");

        if(login != null && password != null) {
            List<Subscriber> subscriberList = subscriberDAO.getByKey(login, where);
            if(subscriberList.size() > 0){
                subscriber = subscriberList.get(0);
            } else {
                request.setAttribute("error", "User not found");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }
        } else {
            logger.info("User not found");
            request.setAttribute("error", "User not found");
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }

        logger.info("Пользователь в бд: " + subscriber.getPassword());
        logger.info("На входе: " + Security.hash(password));

        if(subscriber.getPassword().equals(Security.hash(password))){
            session.setAttribute("user", subscriber);

            List<Tariff> tariffList = getListOfUsersTariffs(subscriber.getLogin());
            session.setAttribute("user_tariffs", tariffList);
            response.sendRedirect("home.jsp");
            logger.info("Id session: " + session.getId());
        } else {
            logger.info("Subscriber = null or invalid password");
            request.setAttribute("error", "Incorrect password");
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
    }

    private List<Tariff> getListOfUsersTariffs(String login){
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();

        List<Tariff> tariffList = tariffDAO.getByKey(login, "inner join providerdb.order on tariff.id_tariff = providerdb.order.tariff where login = ?");
        logger.info("User tariffs: " + tariffList);
        return  tariffList;
    }
}
