package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.security.Security;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddUserCommand implements Command {

	private static Logger logger = LogManager.getLogger(AddUserCommand.class);

	private static String phonePattern = "\\d{10}";

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();

		String login = request.getParameter("user_login");
		String password = Security.hash(request.getParameter("user_password"));
		String name = request.getParameter("user_name");
		String lastName = request.getParameter("user_last_name");
		String phoneNumber = request.getParameter("user_phone_number");
		String status = request.getParameter("user_status");

		if (checkWithRegExp(phonePattern, phoneNumber)) {
			Subscriber subscriber = new Subscriber(login, password, name, lastName, phoneNumber, 0, status, false);

			subscriberDAO.insert(subscriber);
			logger.info("Insert was completed");
			request.setAttribute("user_login", subscriber.getLogin());
			response.sendRedirect("new_user.jsp");
		} else {
			if (checkWithRegExp(phoneNumber, phoneNumber)) {
				request.setAttribute("error", "Invalide phone number!");
			}
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}

	}

	public static boolean checkWithRegExp(String regex, String userNameString) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(userNameString);
		return m.matches();
	}
}
