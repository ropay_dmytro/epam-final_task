package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Дима on 08.02.2018.
 */
public class DefaultRedirect implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.sendRedirect("home.jsp");
    }
}
