package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.objects.Subscriber;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class GetUserListCommand implements Command {

    private static Logger logger = LogManager.getLogger(GetUserListCommand .class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();
        List<Subscriber> subscriberList = subscriberDAO.getAll();
        logger.info(subscriberList);
        request.setAttribute("user_list", subscriberList);
        request.getRequestDispatcher("all_users.jsp").forward(request, response);

    }
}
