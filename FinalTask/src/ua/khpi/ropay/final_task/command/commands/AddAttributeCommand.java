package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.AttributeDAO;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Attribute;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AddAttributeCommand implements Command{

    private static Logger logger = LogManager.getLogger(AddAttributeCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        AttributeDAO attributeDAO = DAOFactory.getAttributeDAO();
        Listener listener = new Listener();
        HttpSession session = request.getSession();
        int id_tariff = Integer.parseInt(request.getParameter("id_tariff"));
        String attributeName = request.getParameter("attribute_name");
        String attributeValue = request.getParameter("attribute_value");

        Tariff tariff = tariffDAO.getByKey(id_tariff + "", "WHERE id_tariff = ?").get(0);

        Attribute attribute = new Attribute(id_tariff, attributeName, attributeValue);
        logger.info("Add attribute: " + attribute);
        attributeDAO.insert(attribute);
        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("tariff.jsp?tariff=" + tariff.getIdService());

    }
}
