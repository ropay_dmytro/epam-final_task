package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.objects.Subscriber;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BlockAndUnblockUserCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();

        String login = request.getParameter("user_login");
        Boolean isBlock = Boolean.valueOf(request.getParameter("is_block"));

        Subscriber subscriber = subscriberDAO.getByKey(login, "WHERE users.login = ?").get(0);

        subscriber.setLocked(isBlock);
        subscriberDAO.update(subscriber);
        response.sendRedirect("Controller?command=get_all_user");

    }
}
