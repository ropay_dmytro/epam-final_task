package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutCommand implements Command {

    private static Logger logger = LogManager.getLogger(LogoutCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.info("Getting session!");
        HttpSession session = request.getSession(false);
        logger.info("Session ID: " + session.getId());


        logger.info("Session prepare to invalidate!");
        session.invalidate();
        logger.info("Session invalidate!");
        response.sendRedirect("home.jsp");
    }
}
