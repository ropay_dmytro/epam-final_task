package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UpdateTariffCommand implements Command {

    private static Logger logger = LogManager.getLogger(UpdateTariffCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        Listener listener = new Listener();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();

        logger.info("Update tariff");
        HttpSession session = request.getSession();
        int idTariff = Integer.parseInt(request.getParameter("id_tariff"));
        int idService = Integer.parseInt(request.getParameter("id_service"));
        String newTariffName = request.getParameter("new_tariff_name");
        int newTariffPrice = Integer.parseInt(request.getParameter("new_tariff_price"));


        Tariff newTariff = new Tariff(idTariff, idService, newTariffName, newTariffPrice);
        tariffDAO.update(newTariff);

        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("tariff.jsp?tariff=" + newTariff.getIdService());

    }
}
