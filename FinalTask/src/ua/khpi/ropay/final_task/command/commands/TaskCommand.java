package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class TaskCommand implements Command {

    private static Logger logger = LogManager.getLogger(TaskCommand.class);

    private String query = "select tariff.name, count(providerdb.order.login) from tariff inner join " +
            "providerdb.order where tariff.id_tariff = providerdb.order.tariff group by name";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Map<String, Integer> result = new TreeMap<>();

        try {
            try (Statement statement = DAOFactory.getConnectionPool().createStatement()) {
                ResultSet resultSet = statement.executeQuery(query);
                result = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("result: " + result);
        request.setAttribute("result", result);
        request.getRequestDispatcher("task_command.jsp").forward(request, response);
    }

    private Map<String, Integer> parseResultSet(ResultSet resultSet) {
        Map<String, Integer> userCount = new TreeMap<>();
        try {
            while (resultSet.next()) {
                userCount.put(resultSet.getString("tariff.name"),
                        resultSet.getInt("count(providerdb.order.login)"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userCount;
    }
}
