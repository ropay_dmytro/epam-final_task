package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AddTariffCommand implements Command {


    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        Listener listener = new Listener();
        HttpSession session = request.getSession();

        String newTariffName = request.getParameter("new_tariff_name");
        int newTariffPrice = Integer.parseInt(request.getParameter("new_tariff_price"));
        int service = Integer.parseInt(request.getParameter("service"));

        Tariff newTariff = new Tariff(service, newTariffName, newTariffPrice);
        tariffDAO.insert(newTariff);

        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("tariff.jsp?tariff=" + service);
    }
}
