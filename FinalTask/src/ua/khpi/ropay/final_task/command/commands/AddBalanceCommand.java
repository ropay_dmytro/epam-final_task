package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.timer_task.MyTimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AddBalanceCommand implements Command {

    private static Logger logger = LogManager.getLogger(AddBalanceCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();
        MyTimerTask myTimerTask = new MyTimerTask();
        HttpSession session = request.getSession();
        String login = request.getParameter("user_login");
        int pay = Integer.parseInt(request.getParameter("pay"));

        if(pay < 0){
            request.getRequestDispatcher("add_balance.jsp").forward(request, response);
        } else {
            Subscriber subscriber = subscriberDAO.getByKey(login, "WHERE users.login = ?").get(0);

            subscriber.setBalance(subscriber.getBalance() + pay);
            subscriberDAO.update(subscriber);

            Subscriber newSubscriber = (Subscriber) session.getAttribute("user");
            newSubscriber.setBalance(subscriber.getBalance());
            session.setAttribute("user", newSubscriber);
            myTimerTask.checkingOrders();
            response.sendRedirect("proffile.jsp");
        }
    }

}
