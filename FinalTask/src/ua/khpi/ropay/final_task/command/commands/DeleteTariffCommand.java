package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.AttributeDAO;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.OrderDAO;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Attribute;
import ua.khpi.ropay.final_task.database.objects.Order;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class DeleteTariffCommand implements Command {
    private static Logger logger = LogManager.getLogger(DeleteTariffCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Listener listener = new Listener();
        HttpSession session = request.getSession();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        AttributeDAO attributeDAO = DAOFactory.getAttributeDAO();
        OrderDAO orderDAO = DAOFactory.getOrderDAO();

        logger.info("Delete tariff");
        int idTariff = Integer.parseInt(request.getParameter("id_tariff"));
        List<Attribute> attributeList = attributeDAO.getByKey(idTariff + "", "WHERE attribute.id_tariff = ?");
        for(Attribute i : attributeList){
            attributeDAO.delete(i.getIdAttribute() + "");
        }

        List<Order> orderList = orderDAO.getByKey(idTariff + "", "WHERE tariff = ?");

        for (Order j : orderList){
            orderDAO.delete(j.getIdOrder() + "");
        }
        tariffDAO.delete(idTariff + "");


        session.setAttribute("user_tariffs", tariffDAO.getAll());

        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("home.jsp");
    }
}
