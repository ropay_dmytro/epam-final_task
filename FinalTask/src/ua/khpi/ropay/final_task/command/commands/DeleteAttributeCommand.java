package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.AttributeDAO;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class DeleteAttributeCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        Listener listener = new Listener();
        HttpSession session = request.getSession();
        AttributeDAO attributeDAO = DAOFactory.getAttributeDAO();

        int idAttribute = Integer.parseInt(request.getParameter("id_attribute"));
        attributeDAO.delete(idAttribute + "");
        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("home.jsp");
    }
}
