package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.context_listener.Listener;
import ua.khpi.ropay.final_task.database.dao.AttributeDAO;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Attribute;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Дима on 05.02.2018.
 */
public class UpdateAttributeCommand implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        AttributeDAO attributeDAO = DAOFactory.getAttributeDAO();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        Listener listener = new Listener();
        HttpSession session = request.getSession();
        int id_attribute = Integer.parseInt(request.getParameter("id_attribute"));
        int id_tariff = Integer.parseInt(request.getParameter("id_tariff"));
        String attributeName = request.getParameter("new_attribute_name");
        String attributeValue = request.getParameter("new_attribute_value");

        Tariff tariff = tariffDAO.getByKey(id_tariff + "", "WHERE id_tariff = ?").get(0);

        Attribute attribute = new Attribute(id_attribute, id_tariff, attributeName, attributeValue);
        attributeDAO.update(attribute);
        listener.loadServiceList(session.getServletContext());
        response.sendRedirect("tariff.jsp?tariff=" + tariff.getIdService());
    }
}
