package ua.khpi.ropay.final_task.command.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.OrderDAO;
import ua.khpi.ropay.final_task.database.dao.SubscriberDAO;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Order;
import ua.khpi.ropay.final_task.database.objects.Subscriber;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * Command class for enable tariff.
 */
public class EnableTariffCommand implements Command {

    private static Logger logger = LogManager.getLogger(EnableTariffCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.info("EnableTariffCommand");
        OrderDAO orderDAO = DAOFactory.getOrderDAO();
        TariffDAO tariffDAO = DAOFactory.getTariffDAO();
        SubscriberDAO subscriberDAO = DAOFactory.getSubscriberDAO();
        HttpSession session = request.getSession();

        int idTariff = Integer.parseInt(request.getParameter("id_tariff"));
        Subscriber subscriber = (Subscriber) session.getAttribute("user");
        Tariff tariff = tariffDAO.getByKey(idTariff + "", "WHERE tariff.id_tariff = ?").get(0);

        if (!checkMatches(subscriber.getLogin(), idTariff)) {
            if (subscriber != null && subscriber.getBalance() - tariff.getPrice() > 0) {
                logger.info("User enabled tariff: " + subscriber.getName());
                subscriber.setBalance(subscriber.getBalance() - tariff.getPrice());
                subscriberDAO.update(subscriber);
                Order order = new Order(subscriber.getLogin(), idTariff, LocalDate.now());
                orderDAO.insert(order);


                List<Tariff> tariffList = (List<Tariff>) session.getAttribute("user_tariffs");
                tariffList.add(tariff);
                session.setAttribute("user_tariffs", tariffList);

                response.sendRedirect("home.jsp");
            } else {
                logger.info(subscriber.getLogin() + " not have money!");
                request.setAttribute("error", "Not have money!");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }
        } else {
            logger.info(subscriber.getLogin() + " tariff was connected!");
            request.setAttribute("error", "Tariff was connected!");
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
    }

    private boolean checkMatches(String login, int tariff) {
        OrderDAO orderDAO = DAOFactory.getOrderDAO();

        List<Order> orderList = orderDAO.getByKey(login, "WHERE login = ? AND tariff = " + tariff);
        logger.info("Order list size : (if > 0 its bad)" + orderList.size());
        if (orderList.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
