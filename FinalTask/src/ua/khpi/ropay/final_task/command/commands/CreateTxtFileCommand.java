package ua.khpi.ropay.final_task.command.commands;

import ua.khpi.ropay.final_task.command.Command;
import ua.khpi.ropay.final_task.database.dao.DAOFactory;
import ua.khpi.ropay.final_task.database.dao.TariffDAO;
import ua.khpi.ropay.final_task.database.objects.Attribute;
import ua.khpi.ropay.final_task.database.objects.Tariff;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

public class CreateTxtFileCommand implements Command {

    private final String filename = "tariffs.txt";
    private final String filename2 = "C:\\Users\\Дима\\git\\epam-final_task\\FinalTask\\";
    int i;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {



        TariffDAO tariffDAO = DAOFactory.getTariffDAO();

        int idService = Integer.parseInt(request.getParameter("id_service"));

        List<Tariff> tariffList = tariffDAO.getByKey(idService + "", "WHERE tariff.id_service = ?");



        try(FileWriter writer = new FileWriter(filename2 + filename, false))
        {
            for (Tariff i : tariffList) {
                writer.write("Tariff: \r\n");
                writer.write("Name: " + i.getName() + " Price: " + i.getPrice() + "\r\nAttributes:");
                for (Attribute j : i.getAttributes()) {
                    writer.write(j.getName() + " - " + j.getValue());
                }
                writer.write("\r\n");
                writer.write("\n");
                writer.write("\r");
            }

            writer.flush();
            writer.close();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");

        java.io.FileInputStream fileInputStream=new java.io.FileInputStream(filename2 + filename);
        ServletOutputStream out = response.getOutputStream();

        while ((i=fileInputStream.read()) != -1) {
            out.write(i);
        }
        fileInputStream.close();


    }

}

