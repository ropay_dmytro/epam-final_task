package ua.khpi.ropay.final_task.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.khpi.ropay.final_task.command.commands.*;

import java.util.Map;
import java.util.TreeMap;

public class Receiver {

    private static Map<String, Command> commands = new TreeMap<>();
    private static Logger logger = LogManager.getLogger(Receiver.class);

    static {
        // common commands
        commands.put("login", new LoginCommand());
        commands.put("add_user", new AddUserCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("update_tariff", new UpdateTariffCommand());
        commands.put("enable_tariff", new EnableTariffCommand());
        commands.put("delete_tariff", new DeleteTariffCommand());
        commands.put("add_tariff", new AddTariffCommand());
        commands.put("get_all_user", new GetUserListCommand());
        commands.put("block_user", new BlockAndUnblockUserCommand());
        commands.put("get_txt_file", new CreateTxtFileCommand());
        commands.put("add_attribute", new AddAttributeCommand());
        commands.put("update_attribute", new UpdateAttributeCommand());
        commands.put("delete_attribute", new DeleteAttributeCommand());
        commands.put("delete_attribute", new DeleteAttributeCommand());
        commands.put("add_balance", new AddBalanceCommand());
        commands.put("task_command", new TaskCommand());
        commands.put("default_redirect", new DefaultRedirect());
    }

    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            logger.info("Not found command!" + commandName);
            return commands.get("default_redirect");
        } else {
            logger.info("Getting command: " + commandName);
            return commands.get(commandName);
        }
    }

}
