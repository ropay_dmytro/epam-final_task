package ua.khpi.ropay.final_task.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Дима on 23.01.2018.
 */
public interface Command {

    void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
